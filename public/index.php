<?php

$participants = json_decode(file_get_contents('data_cars.json'), true);
$attempts = json_decode(file_get_contents('data_attempts.json'), true);

// новый массив с участниками и их результатами
$participants_with_results = [];
foreach ($participants['data'] as $participant) {
    $participants_with_results[$participant['id']] = [
        'id' => $participant['id'],
        'place' => null,
        'name' => $participant['name'],
        'city' => $participant['city'],
        'car' => $participant['car'],
        'results' => [],
        'score' => null
    ];
}

// подсчет итоговых результатов
foreach ($attempts['data'] as $attempt) {
    $participant = $attempt['id'];
    $score = $attempt['result'];
    if (!isset($scores[$participant])) {
        $scores[$participant] = 0;
    }
    $participants_with_results[$participant]['score'] += $score;
}

// запись в массив участников их результата по попыткам
foreach ($attempts['data'] as $result) {
    $participant_id = $result['id'];
    $score = $result['result'];
    $participants_with_results[$participant_id]['results'][] = $score;
}

// сортировка по итоговому значению и баллам в этапах
usort($participants_with_results, static function ($a, $b) {
    if ($a['score'] !== $b['score']) {
        return $b['score'] - $a['score'];
    }

    for ($i = 3; $i >= 1; $i--) {
        if ($a['results'][$i] !== $b['results'][$i]) {
            return $b['results'][$i] - $a['results'][$i];
        }
    }
    return strcmp($a['name'], $b['name']);
});

if (isset($_POST['recalculate'])) {
    $fp = fopen('results.csv', 'w');

    foreach ($participants_with_results as $i => $fields) {
        unset($fields['id']);
        $fields['place'] = $i + 1;
        $fields['results'] = implode(', ', $fields['results']);
        fputcsv($fp, $fields, ';', '"');
    }
    fclose($fp);
}
?>

<!doctype html>
<html lang="ru">
<head>
    <!-- Кодировка веб-страницы -->
    <meta charset="utf-8">
    <!-- Настройка viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>...</title>

    <!-- Bootstrap CSS (Cloudflare CDN) -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/4.6.2/css/bootstrap.min.css"
          integrity="sha512-rt/SrQ4UNIaGfDyEXZtNcyWvQeOq0QLygHluFQcSjaGB04IxWhal71tKuzP6K8eYXYB6vJV4pHkXcmFGGQ1/0w=="
          crossorigin="anonymous" referrerpolicy="no-referrer">
    <!-- jQuery (Cloudflare CDN) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
            integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- Bootstrap Bundle JS (Cloudflare CDN) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/4.6.2/js/bootstrap.bundle.min.js"
            integrity="sha512-igl8WEUuas9k5dtnhKqyyld6TzzRjvMqLC79jkgT3z02FvJyHAuUtyemm/P/jYSne1xwFI06ezQxEwweaiV7VA=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<body>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>Место</th>
        <th>Имя</th>
        <th>Город</th>
        <th>Автомобиль</th>
        <th>Попытка 1</th>
        <th>Попытка 2</th>
        <th>Попытка 3</th>
        <th>Попытка 4</th>
        <th>Сумма очков</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($participants_with_results as $i => $participant): ?>
        <tr>
            <td><?= $i + 1 ?></td>
            <td><?= $participant['name'] ?></td>
            <td><?= $participant['city'] ?></td>
            <td><?= $participant['car'] ?></td>
            <td><?= $participant['results'][0] ?></td>
            <td><?= $participant['results'][1] ?></td>
            <td><?= $participant['results'][2] ?></td>
            <td><?= $participant['results'][3] ?></td>
            <td><?= $participant['score'] ?></td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>
<form method="post">
    <button type="submit" name="recalculate" class="btn btn-primary">Пересчитать результаты</button>
</form>
</body>
</html>
